'use client'
import { FormEvent, useState } from 'react';
import 'tailwindcss/tailwind.css';

export default function Page() {
  const query = '{\"query\": \"query {staffs {name email phone}}\"}';
  const [staffs, setStaffs] = useState([]);
  const [form, setForm] = useState({
    name: "",
    email: "",
    phone: "",
    address: "",
    birthdate: "",
    gender: ""
  });
  
  async function onSubmit(event) {
    event.preventDefault();

    console.log(form);

    const mutvar = JSON.stringify({
      query: `mutation ($input: StaffInput) {
        staff(input: $input) {
          name
          email
        }
      }`,
      variables: {
        input: {
          name: form.name,
          email: form.email,
          phone: form.phone
        }
      }
    });

    const response = await fetch('http://localhost:4000/graphql', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: mutvar,
    });

    const rtn = await response.json();
    console.log(rtn);
  }

  function onChange(event) {
    setForm({
      ...form,
      [event.target.name]: event.target.value
    });
  }

  const temp = fetch('http://localhost:4000/graphql', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: query
  })
  .then((res) => res.json())
  .then(json => {
    setStaffs(json.data.staffs);
  });

  return (
    <div>
      <form onSubmit={onSubmit}>
        <input className="bg-gray-50 border" type="text" name="name" value={form.name} onChange={onChange} placeholder="Нэр" />
        <input className="bg-gray-50 border" type="text" name="email" value={form.email} onChange={onChange} placeholder="Имэйл" />
        <input className="bg-gray-50 border" type="text" name="phone" value={form.phone} onChange={onChange} placeholder="Утас"/>
        <input className="bg-gray-50 border" type="text" name="address" value={form.address} onChange={onChange} placeholder="Хаяг" />
        <input className="bg-gray-50 border" type="text" name="birthdate" value={form.birthdate} onChange={onChange} placeholder="Төрсөн өдөр" />
        <input className="bg-gray-50 border" type="text" name="gender" value={form.gender} onChange={onChange} placeholder="Хүйс" />
        <button className="bg-blue-500 rounded-full text-white font-bold py-2 px-4" type="submit">Бүртгэх</button>
      </form>
      <div>
        <div className="grid gap-4 grid-cols-3">
          <span>Нэр</span>
          <span>Имэйл</span>
          <span>Утас</span>
        </div>
        {staffs.map((staff, index) => {
          return (
            <div className="grid gap-4 grid-cols-3" key={index}>
              <span>{staff.name}</span>
              <span>{staff.email}</span>
              <span>{staff.phone}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
}